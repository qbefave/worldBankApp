import React from "react";

function Table(props) {
    const {data} = props;

    return (
        <div>
            <div>
                <h2>{data[0].countryName}</h2>
                <h2>{data[0].indicatorName}</h2>
                </div>
                <table>
                    <thead>
                    <tr>
                        <th>Year</th>
                        <th>Value</th>
                    </tr>
                    </thead>
                    <tbody>
                    {data.map((value) => (
                        <tr>
                            <td>{value.year}</td>
                            <td>{value.value}</td>
                        </tr>
                    ))}

                    </tbody>
                </table>
                </div>
                );
                }

                export default Table;