import React, {useCallback} from 'react';
import FileSaver from "file-saver";
import { useCurrentPng } from "recharts-to-png";
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';



function PieCharts(props) {
    const { data } = props;

    const [getPng, { ref, isLoading }] = useCurrentPng();
    const handleDownload = useCallback(async () => {
        const png = await getPng();
        if (png) {
            FileSaver.saveAs(png, 'myChart.png');
        }
    }, [getPng]);

    return (
        <div>
            <div>
                <h2>{data[0].countryName}</h2>
                <h2>{data[0].indicatorName}</h2>
            </div>
            <div style={{ width: "100%", height: 300 }}>
                <ResponsiveContainer>
                    <PieChart ref = {ref}>
                        <Pie  data={data} fill="#ffa500" datakey = "value" namekey = "year" innerRadius={70} label/>
                        <Tooltip />
                    </PieChart>
                </ResponsiveContainer>
            </div>
            <div>
                <button onClick={handleDownload}>
                    {isLoading ? 'Downloading...' : 'Download Chart'}
                </button>
            </div>
        </div>
    );
}

export default PieCharts;