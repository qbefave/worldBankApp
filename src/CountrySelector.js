import React, {useState, useEffect} from 'react';
// for API fetching
import axios from 'axios';
// import chart components
import AreaCharts from "./AreaCharts";
import BarCharts from "./BarCharts";
import Table from "./Table";
import PieChars from "./PieChars";



const testToogle = (data1) => {
    return data1.map(item => {
        var temp = Object.assign({}, item);
        temp.value = parseFloat(temp.value);
        return temp;
    });
}

function CountrySelector() {

    const [countries, setCountries] = useState([]);
    const [group, setGroup] = useState([]);
    const [pickedGroupName, setPickedGroupName] = useState(false);
    const [indicator, setIndicator] = useState([]);
    const [startYear, setStartYear] = useState([]);
    const [endYear, setEndYear] = useState([]);
    const [pickedInd, setPickedInd] = useState([]);

    const [pickedChart, setPickedChart] = useState([]);
// $countryData1 & on: data for user-chosen country
    const [countryData, setCountryData] = useState([]);


// table data
    const [curVal, setCurVal] = useState([]);

    const types = [
        'none',
        'table',
        'line chart',
        'bar chart',
        'pie chart'
    ]

    // when component is about to mount, fetch the full country list
    // and store the list in the 'countries' state http://localhost:8080/country
    useEffect(() => {

        axios.get(
            'http://localhost:8080/country',
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                },
            }
        ).then(res => {

            const countryList = res.data.data;

            setCountries(countryList)
            console.log(countryData)
        })
            .catch(error => console.log(error));
    }, []);
// group
    useEffect(() => {

        //axios.get('http://178.208.86.127:8080/groups',
        axios.get(
            'http://localhost:8080/groups',
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                },
            }
        ).then(res => {

            const groupList = res.data.data;

            setGroup(groupList)
        })
            .catch(error => console.log(error));
    }, []);

    useEffect(() => {

        //axios.get('http://178.208.86.127:8080/groups',
        axios.get(
            'http://localhost:8080/indicators',
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                    'Content-Type': 'application/json',
                },
            }
        ).then(res => {

            const indList = res.data.data;

            setIndicator(indList)
        })
            .catch(error => console.log(error));
    }, []);

    // on select change (user changes dropdown):
    // (1) set errors to false
    // (2) fetch, clean, and store data to states
    const storeChoiceType = type => {
        setPickedChart(type);
        console.log(type)
    };


    const storeChoiceGroup = gr => {
        setPickedGroupName(gr);
        console.log(pickedGroupName)

    };
    const storeChoiceInd = ind => {
        setPickedInd(ind);
    };
    return (
        <div>
            <div className="dropdown-container">
                {countryData.length === 0 ?
                    <p>
                        Pick a country/group/indicator to start:</p>
                    : <p>
                        Pick another country/group/indicator:</p>}
                <div style={{height: "150px", width: "200px"}}>
                    <table>
                    <tr>
                        <td>
                            <select
                                className="dropdown-country"
                                name="dropdown-country"
                                defaultValue={'none'}
                                onChange={cnt => {
                                    setCountryData(cnt.target.value)
                                }}>

                                <option
                                    value="none"
                                    disabled
                                    hidden>
                                    set country
                                </option>

                                {countries.map(country =>
                                    <option
                                        key={country.id}
                                        value={country.id}>
                                        {country.fullName}
                                    </option>)
                                }
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <select

                                className="dropdown-group"
                                name="dropdown-group"
                                defaultValue={'none'}
                                onChange={gr => storeChoiceGroup(gr.target.value)}>

                                <option
                                    value="none"
                                    disabled
                                    hidden>
                                    set group
                                </option>

                                {group.map(group =>
                                    <option
                                        key={group.id}
                                        value={group.name}>
                                        {group.name}
                                    </option>)
                                }
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <select

                                className="dropdown-indicators"
                                name="dropdown-indicators"
                                defaultValue={'none'}
                                onChange={ind => storeChoiceInd(ind.target.value)}>

                                <option
                                    value="none"
                                    disabled
                                    hidden>
                                    set indicator
                                </option>

                                {indicator.map(indicator =>
                                    <option
                                        key={indicator.id}
                                        value={indicator.id}>
                                        {indicator.name}
                                    </option>)
                                }
                            </select>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <input
                                className="yearInput"
                                type="number"
                                placeholder="from"
                                pattern="[0-9]"
                                maxLength="4"
                                min="1990"
                                max="2021"
                                value={startYear}
                                onChange={yr => setStartYear(yr.target.value)}

                            />
                            <input
                                className="yearInput"
                                type="number"
                                placeholder="to"
                                pattern="[0-9]"
                                maxLength="4"
                                min="1990"
                                max="2021"
                                value={endYear}
                                onChange={yr => setEndYear(yr.target.value)}

                            />
                        </td>
                    </tr>
                </table>
                </div>


                <p>Enter starting and ending year</p>



                <button
                    class="button-5" role="button"
                    disabled={startYear.length === 0 && endYear.length === 0}
                    type="button"
                    onClick={
                        (async () => {
                            const rawResponse = await fetch('http://localhost:8080/' + pickedGroupName.replace(/\s/g, ''), {
                                method: "POST",
                                headers: {
                                    'Access-Control-Allow-Origin': '*',
                                    'Content-Type': 'application/json',
                                    'Accept': 'application/json'
                                },
                                body: JSON.stringify({
                                    id_country: countryData,
                                    id_indicator: pickedInd,
                                    startYear: startYear,
                                    endYear: endYear
                                })
                            });
                            const content = await rawResponse.json()
                            const prepData = testToogle(content.data)
                            setCurVal(prepData)
                        })
                    }
                    style={{
                        margin: 20,
                        padding: 20,
                        backgroundColor: "orange",
                        color: "black",
                        cursor: "pointer"
                    }}
                >
                    Use filter
                </button>

                <select
                    className="dropdown-type"
                    name="dropdown-type"
                    defaultValue={'none'}
                    onChange={type => storeChoiceType(type.target.value)}>

                    <option
                        value="none"
                        disabled
                        hidden>
                        set type render
                    </option>

                    {types.map(types =>
                        <option
                            key={types.id}
                            value={types}>
                            {types}
                        </option>)
                    }
                </select>

                {curVal === undefined || curVal.length === 0 ?
                    null
                    : pickedChart === "table" ?

                        <div>
                            <Table
                                data={curVal}>
                            </Table>
                        </div>

                        : pickedChart === 'line chart' ?
                            curVal === undefined || curVal.length === 0 ?
                                null
                                :
                                <div>
                                    <AreaCharts
                                        data={curVal}>
                                    </AreaCharts>
                                </div>
                            : pickedChart === "bar chart" ?


                                curVal === undefined || curVal.length === 0 ?
                                    null
                                    :
                                    <div>
                                        <BarCharts
                                            data={curVal}>
                                        </BarCharts>
                                    </div>

                                :pickedChart === "pie chart" ?


                                    curVal === undefined || curVal.length === 0 ?
                                        null
                                        :
                                        <div>
                                            <PieChars
                                                data={curVal}>
                                            </PieChars>
                                        </div>

                                    :
                                <p>no data yet</p>
                }
            </div>
        </div>
    )
        ;

}


export default CountrySelector;
/**/