import React, {useCallback} from 'react';
import FileSaver from "file-saver";
import { useCurrentPng } from "recharts-to-png";
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer } from 'recharts';



function BarCharts(props) {
    const { data } = props;

    const [getPng, { ref, isLoading }] = useCurrentPng();
    const handleDownload = useCallback(async () => {
        const png = await getPng();
        if (png) {
            FileSaver.saveAs(png, 'myChart.png');
        }
    }, [getPng]);

    return (
        <div>
            <div>
                <h2>{data[0].countryName}</h2>
                <h2>{data[0].indicatorName}</h2>
            </div>
            <div style={{ width: '85%', height: 400, margin: '0 auto' }}>
                <ResponsiveContainer>
                    <BarChart data={data} ref = {ref}>
                        <defs>
                            <linearGradient id="colorUv" x1="0" y1="0" x2="0" y2="1">
                                <stop offset="5%" stopColor="#ffa500" stopOpacity={0.8} />
                                <stop offset="95%" stopColor="#ffa500" stopOpacity={0.15} />
                            </linearGradient>
                        </defs>
                        <XAxis
                            reversed dataKey="year" />

                        <YAxis />

                        <CartesianGrid
                            strokeDasharray="3 3" />

                        <Tooltip />

                        <Bar animationDuration={1750} type="monotone"
                             dataKey="value"
                             stroke="#022d5b" fillOpacity={1} fill="url(#colorUv)" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
            <div style={{padding: 20}}>
                <button onClick={handleDownload}>
                    {isLoading ? 'Downloading...' : 'Download Chart'}
                </button>
            </div>
        </div>
    );
}

export default BarCharts;