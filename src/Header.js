import React from 'react';

const Header = () => {
    return (
        <div className="header">
            <header className="App-header">
                <h1>
                    Web-приложение для анализа открытых данных Всемирного банка
                </h1>
                <p>
                    Frontend:<br /> Заречная Юлия, Барашкин Дмитрий<br />
                    Backend:<br /> Прохоров Андрей, Фатеева Анастасия, Грунин Иван<br />
                </p>

            </header>
        </div>
    );
}

export default Header;