import React, { Component } from 'react';
import './App.css';
//import DropDown from './DropDown.js';
import CountrySelector from "./CountrySelector";
import Header from "./Header";


function App() {

  return (
//    <div className="App">
  //      <DropDown />
  //    </div>
      <div className="App">
          <Header />
          <CountrySelector />

      </div>
  );
}

export default App;